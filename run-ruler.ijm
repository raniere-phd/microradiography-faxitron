/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will calculate information from the region of interest (ROI)
for transverse section with circular shape:
transverse section of the mid-diaphysis of the left tenth rib,
transverse section of the distal third region of the right tibia,
and
transverse section of the lateral distal metaphysis of the right third metacarpal bone.
*/

run("Input/Output...", "jpeg=85 gif=-1 file=.csv use_file copy_row save_column");
setForegroundColor(0, 102, 255);
run("Line Width...", "line=3");

headless_str_arg = getArgument();

headless_args = split(headless_str_arg)

if(headless_args.length == 0) {
    root_path = getDirectory("Choose root directory");

    // TODO Dialog window
    size_min = 0;
    size_max = 'Infinity';
    circularity_min = 0.0;
    circularity_max = 1.0;
    threshold = 'Triangle';
}
else {
    if(headless_args[0].endsWith("/")) {
        root_path = headless_args[0];
    }
    else {
        root_path = headless_args[0] + "/";
    }

    size_min = headless_args[1];
    size_max = headless_args[2];
    circularity_min = headless_args[3];
    circularity_max = headless_args[4];
    threshold = headless_args[5];
}

source_path = root_path + "data/";
destination_path = root_path + "data-augmented/";

print("Batch processing images");
print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);

print("Parameters");
print("Particle min size: " + size_min);
print("Particle max size: " + size_max);
print("Particle min circularity: " + circularity_min);
print("Particle max circularity: " + circularity_max);

bones_slugs = newArray(
    'test/',
    'left-10th-rib/',
    'left-tibia-distal-third/',
    'right-metacarpal-3-mid-diaphyseal/',
    'left-proximal-humerus-proximal-to-deltoid-tuberosity/',
    'right-metacarpal-3-distal-diaphysis-lateral-aspect/'
);

for(i=0; i<bones_slugs.length; i++) {

    if(endsWith(bones_slugs[i], "/")) {
	print("Processing directory " + bones_slugs[i]);

	File.makeDirectory(destination_path + bones_slugs[i])

	horses = getFileList(source_path + bones_slugs[i]);

	for(j=0; j<horses.length; j++) {
	    if(endsWith(horses[j], ".tif")) {

		roiManager("reset");

		print("Processing file " + bones_slugs[i] + horses[j]);

		open(source_path + bones_slugs[i] + horses[j]);

		roi_filename = source_path + bones_slugs[i] + horses[j];
		roi_filename = replace(roi_filename, ".tif", ".periosteum.roi");
		roiManager(
		    "Open",
		    roi_filename
		);
		roi_filename = source_path + bones_slugs[i] + horses[j];
		roi_filename = replace(roi_filename, ".tif", ".endosteum.roi");
		roiManager(
		    "Open",
		    roi_filename
		);

		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");

		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".png")
		);

		run("Duplicate...", "title=compact.bone.xray.png ignore");
		run("RGB Color");
		run("Select None");
		roiManager("Deselect");
		roiManager("XOR");
		if(selectionType == -1) { // if there is no selection
		    roiManager("Select", 0);
		}
		run("Draw", "slice");
		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		run("Flatten");  // Creates a new window
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".compact.bone.xray.png")
		);
		close();  // Close flatten window
		close();  // Close compact.bone.xray.png

		run("Duplicate...", "title=binary.png ignore");

		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		roiManager("XOR");
		if(selectionType == -1) { // if there is no selection
		    roiManager("Select", 0);
		}

		setAutoThreshold(threshold);
		setOption("BlackBackground", true);
		run("Convert to Mask");

		// Invert images for archive. MUST undo later before run Particle Analyze...
		run("Select All");
		run("Invert");

		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".binary.png")
		);

		run("Duplicate...", "title=compact.bone.png ignore");
		run("Select None");
		roiManager("Deselect");
		roiManager("XOR");
		if(selectionType == -1) { // if there is no selection
		    roiManager("Select", 0);
		}
		run("Set Measurements...", "area center display redirect=None decimal=3");
		run("Measure");  // Calculate compact bone area
		saveAs(
		    "Results",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".compact.bone.csv")
		);
		run("Clear Results");
		run("RGB Color");
		run("Draw", "slice");
		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		run("Flatten");  // Creates a new window
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".compact.bone.binary.png")
		);
		close();  // Close flatten window
		close();  // Close compact.bone.png


		run("Duplicate...", "title=compact.bone.area.png ignore");
		run("Select None");
		roiManager("Deselect");
		roiManager("XOR");
		if(selectionType == -1) { // if there is no selection
		    roiManager("Select", 0);
		}
		run("RGB Color");
		run("Fill", "slice");
		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		run("Flatten");  // Creates a new window
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".compact.bone.area.png")
		);
		close();  // Close flatten window
		close();  // Close compact.bone.area.png

                // For inverting LUT,
		// Particle Analyse only count black dots (pixel value = 255) in white background (pixel value = 0).
                // We need to UNDO invert the colour of the image.
		run("Select All");
		run("Invert");

		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		roiManager("XOR");
		if(selectionType == -1) { // if there is no selection
		    roiManager("Select", 0);
		}

		run("Set Measurements...", "area center shape display redirect=None decimal=3");
		run("Analyze Particles...", "size=" + size_min + "-" + size_max + " pixel circularity=" + circularity_min + "-" + circularity_max + " show=Ellipses display exclude clear include add");
		saveAs(
		    "Results",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".resorption.canal.csv")
		);
		run("Clear Results");
                roiManager(
                    "Save",
                    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".resorption.canal.zip")
                );
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".resorption.canal.ellipses.png")
		);
                close();  // Closes resorption.canal.ellipses.png

		// Invert the colour back to original.
		run("Select None");
                roiManager("Deselect");
		roiManager("Show None");
		run("Select All");
		run("Invert");

		run("Duplicate...", "title=resorption.canal.binary.png ignore");
		run("RGB Color");
		for(k=0; k<roiManager("count"); k++) {
    		    roiManager("Select", k);
		    run("Fill", "slice");
		}
		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		run("Flatten");  // Creates a new window
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".resorption.canal.binary.png")
		);
		close();  // Closes flatten window
		close();  // Closes resorption.canal.binary.png

		close();  // Closes binary.png

		// Back to original image
		run("RGB Color");
		for(k=0; k<roiManager("count"); k++) {
    		    roiManager("Select", k);
		    run("Draw", "slice");
		}
		run("Select None");
		roiManager("Deselect");
		roiManager("Show None");
		run("Flatten");  // Creates a new window
		saveAs(
		    "PNG",
		    destination_path + bones_slugs[i] + replace(horses[j], ".tif", ".resorption.canal.xray.png")
		);
		close();  // Closes flatten window
		close();  // Closes original window

	    }
	    else {
		print("Skipping " + horses[j]);
	    }
	}
    }
    else {
	print("Skipping " + bones_slugs[i]);
    }
}

print("End of batch process");
