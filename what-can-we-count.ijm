/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will create illustration to help answer the question
"What can we count?"
*/

headless_str_arg = getArgument();

headless_args = split(headless_str_arg)

if(headless_args.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_args[0].endsWith("/")) {
        root_path = headless_args[0];
    }
    else {
        root_path = headless_args[0] + "/";
    }
}

source_path = root_path + "data/";
destination_path = root_path + "img/";

print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);

open(source_path + "left-tibia-distal-third/3004.tif");

makeRectangle(789, 764, 150, 150);
run("Crop");

makeOval(139, 49, 3, 3);
roiManager("Add");
makeOval(3, 24, 6, 6);
roiManager("Add");
makeOval(68, 47, 13, 12);
roiManager("Add");

roiManager("Select", 0);
RoiManager.setGroup(0);
RoiManager.setPosition(0);
roiManager("Set Color", "red");
roiManager("Set Line Width", 1);
roiManager("Select", 1);
RoiManager.setGroup(0);
RoiManager.setPosition(0);
roiManager("Set Color", "yellow");
roiManager("Set Line Width", 1);
roiManager("Select", 2);
RoiManager.setGroup(0);
RoiManager.setPosition(0);
roiManager("Set Color", "green");
roiManager("Set Line Width", 1);

roiManager("Show All without labels");

run("Flatten");

run("Scale Bar...", "width=1 height=4 font=14 color=White background=None location=[Lower Right] bold overlay");

saveAs(
    "PNG",
    destination_path + 'what-can-we-count-2x.png'
);
close();

print("Illustrations saved!");

if(headless_args.length == 0) {
    exit;
}
else {
    run("Quit");
}
