"""
MLflow Tracking Script for Microradiography Faxitron
"""
import os
import subprocess

import nbformat
from nbconvert.preprocessors import ExecutePreprocessor

import click

import mlflow

@click.command()
@click.option('--gui', default=0, type=int, help='Minimum size for particles in pixels.')
@click.option('--size-min', default=0, type=str, help='Minimum size for particles in pixels.')
@click.option('--size-max', default='Infinity', type=str, help='Maximum size for particles in mm^2.')
@click.option('--circularity-min', default=0.0, type=str, help='Minimum value for circularity.')
@click.option('--circularity-max', default=1.0, type=str, help='Maximum value for circularity.')
@click.option('--threshold', default='Triangle', type=str, help='Method to determine the threshold.')
def run_experiment(gui, size_min, size_max, circularity_min, circularity_max, threshold):
    """Call ImageJ to identify resorption canals using Analyse Particles..."""
    # Log parameteres

    # Perform experiment
    subprocess.run(
        [
            'ImageJ',
            '-macro' if gui else '-batch',
            'run-ruler.ijm',
            '{} {} {} {} {} {}'.format(
                os.getcwd(),
                size_min,
                size_max,
                circularity_min,
                circularity_max,
                threshold
            )
        ]
        # check=True
    )

    # Log metrics as HTML Jupyter Notebook
    subprocess.run(
        [
            'make',
            '-B'
        ],
        check=True
    )

    # Log artifacts
    mlflow.log_artifacts("data-augmented")
    mlflow.log_artifact("report.ipynb")
    mlflow.log_artifact("report.nbconvert.html")
    mlflow.log_artifact("benchmark.ipynb")
    mlflow.log_artifact("benchmark.nbconvert.html")

if __name__ == "__main__":
    run_experiment()
