---
name: Bug report
about: Create a report to help us improve
---

Depends on:

Blocks: 

# Description

# Known commit with this bug

```
$ git rev-parse HEAD
```

# Minimum Steps to Reproduce


# Observed Result/Output

# Expected Result/Output

# Environment

```
$ ImageJ --headless --console -eval "print(getVersion());" | tail -n 1
```