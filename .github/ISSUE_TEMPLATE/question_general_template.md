---
name: Question
about: Create question regarding the project
---

Depends on:

Blocks: 

# Question

# Known commit with this bug

```
$ git rev-parse HEAD
```

# Minimum Steps to Reproduce

# Environment

```
$ ImageJ --headless --console -eval "print(getVersion());" | tail -n 1
```