RAW_TIF := $(sort $(shell find data-raw -type f -name '*.tif'))
SVG_FROM_TIF := $(sort $(shell find data-raw -type f -name '*.svg'))
PNG_FROM_SVG := $(SVG_FROM_TIF:.svg=.svg.to.mask.png)
PNG_TO_ROI := $(SVG_FROM_TIF:.svg=.mask.for.roi.png)
ROI_PERIOSTEUM := $(SVG_FROM_TIF:.svg=.periosteum.roi)
ROI_ENDOSTEUM := $(SVG_FROM_TIF:.svg=.endosteum.roi)
ALL_ROI := $(ROI_PERIOSTEUM) $(ROI_ENDOSTEUM)

.PHONY: spell data data/test

.PRECIOUS: $(PNG_FROM_SVG)

all: report.nbconvert.html report.nbconvert.slides.html benchmark.nbconvert.html article.html

spell: article.Rmd
	hunspell -d en_GB,en_med $<

article.html: article.Rmd
	$(MAKE) -C img
	Rscript -e 'bookdown::render_book("$<", "bookdown::html_document2")' && mv _main.html $@

article.pdf: article.Rmd
	$(MAKE) -C img
	Rscript -e 'bookdown::render_book("$<", "bookdown::pdf_book")' && mv _book/_main.pdf $@

article.docx: article.Rmd
	$(MAKE) -C img
	Rscript -e 'bookdown::render_book("$<", "bookdown::word_document2")' && mv _book/_main.docx $@

%.nbconvert.ipynb: %.ipynb
	jupyter nbconvert -y --execute --to notebook $<

%.nbconvert.html: %.nbconvert.ipynb
	jupyter nbconvert -y --to html_embed $<

%.nbconvert.slides.html: %.nbconvert.ipynb
	jupyter nbconvert -y --to slides $<

report.ipynb: intro.ipynb data-and-functions.ipynb validation.ipynb left-10th-rib.ipynb left-tibia-distal-third.ipynb right-metacarpal-3-mid-diaphyseal.ipynb left-proximal-humerus-proximal-to-deltoid-tuberosity.ipynb right-metacarpal-3-distal-diaphysis-lateral-aspect.ipynb polar-coordinates.ipynb conclusion.ipynb
	nbmerge $^ > $@

left-tibia-distal-third.ipynb right-metacarpal-3-mid-diaphyseal.ipynb left-proximal-humerus-proximal-to-deltoid-tuberosity.ipynb right-metacarpal-3-distal-diaphysis-lateral-aspect.ipynb: left-10th-rib.ipynb
	cp $^ $@
	sed -i 's/left-10th-rib/$(@:.ipynb=)/g' $@

%.svg.to.mask.png: %.svg
	inkscape \
		--without-gui \
		--export-filename=$@ \
		--export-overwrite \
		--export-type=png \
		--export-area-page \
		--export-width=2368 \
		--export-height=2340 \
		$<

%.mask.for.roi.png: %.svg.to.mask.png
	@echo You need to edit $< and create $@

%.periosteum.roi %.endosteum.roi: %.mask.for.roi.png mask-to-roi.ijm
	fiji -batch mask-to-roi.ijm $(CURDIR)/$<

data: $(RAW_TIF) $(ALL_ROI) fix-scale.ijm
	fiji -batch fix-scale.ijm $(CURDIR)

data/test: create-close-up-for-test.ijm
	fiji -batch create-close-up-for-test.ijm $(CURDIR)
