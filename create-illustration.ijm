/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will create illustration of the microradiography for publication.

How to run:

$ fiji -batch create-illustration.ijm /path/to/microradiography-faxitron/
*/

headless_str_arg = getArgument();

headless_args = split(headless_str_arg)

if(headless_args.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_args[0].endsWith("/")) {
        root_path = headless_args[0];
    }
    else {
        root_path = headless_args[0] + "/";
    }
}

source_path = root_path + "data/";
destination_path = root_path + "img/";

print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);

radiograph_slugs = getFileList(source_path);

for(i=0; i<radiograph_slugs.length; i++) {

    if(endsWith(radiograph_slugs[i], "/")) {

	File.makeDirectory(destination_path + radiograph_slugs[i])

        image_filenames = getFileList(source_path + radiograph_slugs[i]);

        for(j=0; j<image_filenames.length; j++) {

            if(endsWith(image_filenames[j], ".tif")) {

	        open(source_path + radiograph_slugs[i] + image_filenames[j]);

	        run("Duplicate...", " ");

		run("Canvas Size...", "width=" + getWidth() + " height=" + (getHeight() + 160) + " position=Bottom-Right");
		new_image_filename = destination_path + radiograph_slugs[i] + replace(image_filenames[j], '.tif', '-scale-bar.png');
	        run("Scale Bar...", "width=5 height=80 font=84 color=White background=None location=[Upper Right] bold hide overlay");
	        saveAs(
	            "PNG",
		    new_image_filename
	        );
	        close();
		print("Created " + new_image_filename);

		run("Canvas Size...", "width=" + getWidth() + " height=" + (getHeight() + 160) + " position=Bottom-Right");
		new_image_filename = destination_path + radiograph_slugs[i] + replace(image_filenames[j], '.tif', '-scale-bar-with-text.png');
	        run("Scale Bar...", "width=5 height=80 font=84 color=White background=None location=[Upper Right] bold overlay");
	        saveAs(
	            "PNG",
		    new_image_filename
	        );
	        close();
		print("Created " + new_image_filename);
            }
            else {
	        print("Skipping " + destination_path + radiograph_slugs[i] + image_filenames[j]);
            }
        }

    }
    else {
	print("Skipping " + destination_path + radiograph_slugs[i]);
    }
}

print("Illustrations saved!");
