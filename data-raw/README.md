# Faxitron Bone Sections

## Author

| Dr. Christopher T Sun BVSc MRCVS
| Intern,  Equine Hospital,
| The Hong Kong Jockey Club.

## Case Definitions

Study horses (n=12) were assigned to one of two groups:
exercised (n=6)
and
rested (n=6).

The Exercised Group was defined by having had a recorded period
of intense exercise within seven days of death.
Intense exercise was defined as a race,
barrier trial, training gallop or training canter
on at least four occasions in the preceding 30 days. 
The Rested Group was defined by having had a similar period
of intense exercise to that of the Exercised Group
but was either being rested
or was in retirement for one to four months since that period of exercise.  
 
 