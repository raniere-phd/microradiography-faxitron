/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will fix the scale of the images.
*/

run("Input/Output...", "jpeg=85 gif=-1 file=.csv use_file copy_row save_column");

headless_arg = getArgument();

if(headless_arg.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_arg.endsWith("/")) {
        root_path = headless_arg;
    }
    else {
        root_path = headless_arg + "/";
    }
}

source_path = root_path + "data-raw/";
destination_path = root_path + "data/";

print("Batch processing images to fix scale");
print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);

files = getFileList(source_path);

for(i=0; i<files.length; i++) {
    if(endsWith(files[i], "/")) {
	print("Processing directory " + files[i]);

	File.makeDirectory(destination_path + files[i])

	subfiles = getFileList(source_path + files[i]);

	for(j=0; j<subfiles.length; j++) {
	    if(endsWith(subfiles[j], ".tif")) {
		print("Processing image " + files[i] + subfiles[j]);

		open(source_path + files[i] + subfiles[j]);

                if(endsWith(subfiles[j], "5x.tif")) {
                    run("Set Scale...", "distance=1 known=0.0100 unit=mm");
                }
                if(endsWith(subfiles[j], "4x.tif")) {
                    run("Set Scale...", "distance=1 known=0.0125 unit=mm");
                }
                if(endsWith(subfiles[j], "3x.tif")) {
                    run("Set Scale...", "distance=1 known=0.0167 unit=mm");
                }
                if(endsWith(subfiles[j], "2x.tif")) {
                    run("Set Scale...", "distance=1 known=0.0250 unit=mm");
                }

		saveAs(
		    "Tiff",
		    destination_path + files[i] + substring(subfiles[j], 0, 2) + ".tif"
		);

		close();
	    }
	    else if(indexOf(subfiles[j], ".periosteum.roi") >= 0) {
		File.copy(
		    source_path + files[i] + subfiles[j],
		    destination_path + files[i] + substring(subfiles[j], 0, 2) + ".periosteum.roi"
		)
		print("Copied " + subfiles[j]);
	    }
	    else if(indexOf(subfiles[j], ".endosteum.roi") >= 0) {
		File.copy(
		    source_path + files[i] + subfiles[j],
		    destination_path + files[i] + substring(subfiles[j], 0, 2) + ".endosteum.roi"
		)
		print("Copied " + subfiles[j]);
	    }
	    else if(indexOf(subfiles[j], ".periosteum.csv") >= 0) {
		File.copy(
		    source_path + files[i] + subfiles[j],
		    destination_path + files[i] + substring(subfiles[j], 0, 2) + ".periosteum.csv"
		)
		print("Copied " + subfiles[j]);
	    }
	    else if(indexOf(subfiles[j], ".endosteum.csv") >= 0) {
		File.copy(
		    source_path + files[i] + subfiles[j],
		    destination_path + files[i] + substring(subfiles[j], 0, 2) + ".endosteum.csv"
		)
		print("Copied " + subfiles[j]);
	    }
	    else if(endsWith(subfiles[j], ".csv")) {
		File.copy(
		    source_path + files[i] + subfiles[j],
		    destination_path + files[i] + subfiles[j]
		)
		print("Copied " + subfiles[j]);
	    }
	    else {
		    print("Skipping " + subfiles[j]);
	    }
	}
    }
    else {
	if(endsWith(files[i], ".csv")) {
	    File.copy(
		source_path + files[i],
		destination_path + files[i]
	    )
	    print("Copied " + files[i]);
	}
	else {
	    print("Skipping " + files[i]);
	}
    }
}

print("End of batch process");

if(headless_arg.length == 0) {
    exit;
}
else {
    run("Quit");
}
