/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will create ROI for the image.
*/

run("Set Measurements...", "area center display redirect=None decimal=3");

headless_str_arg = getArgument();

headless_args = split(headless_str_arg);

if(headless_args.length == 0) {
    file_path = File.openDialog("Choose file");
}
else {
    file_path = headless_args[0];
}

source_path = File.getDirectory(file_path);
destination_path = File.getDirectory(file_path);
filename = File.getName(file_path);
basename = split(filename, '.');
basename = basename[0];

periosteum_roi_path = destination_path + basename + ".periosteum.roi";
endosteum_roi_path = destination_path + basename + ".endosteum.roi";

periosteum_csv = destination_path + basename + ".periosteum.csv";
endosteum_csv = destination_path + basename + ".endosteum.csv";

print("Processing " + file_path);

open(file_path);

run("8-bit");

setAutoThreshold("Default dark");
setOption("BlackBackground", false);
run("Convert to Mask");

run("Analyze Particles...", "display clear include add");

saveAs(
    "Results",
    periosteum_csv
);
run("Clear Results");
print("Saved " + periosteum_csv);

roiManager("Select", 0);
roiManager(
    "Save",
    periosteum_roi_path
);
print("Saved " + periosteum_roi_path);

roiManager("Deselect");
run("Select All");
run("Invert");

roiManager("Select", 0);

run("Analyze Particles...", "display clear include add");

saveAs(
    "Results",
    endosteum_csv
);
run("Clear Results");
print("Saved " + periosteum_csv);

roiManager("Select", 0);
roiManager(
    "Save",
    endosteum_roi_path
);
print("Saved " + endosteum_roi_path);
