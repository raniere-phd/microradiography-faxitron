import math
import os.path

import numpy as np
import scipy.stats
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as mpatches
import seaborn as sns

import roifile

from matplotlib import rcParams

# Configuring libraries
sns.set_theme(font='sans-serif')
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['DejaVu Sans']  # To resolve superscript minus (glyph 8315) missing from current font.
def plot_resorption_canal_area_histogram(df, bone_directory_name):
    """Visualise canal area for bone"""
    for log_scale in [False, True]:
        plt.figure()
        ax = sns.histplot(
            data=df,
            x="Area",
            hue="Group",
            bins=10,
            log_scale=log_scale
        )
        ax.set_title(
            'Canal Area (mm²) for {}{}'.format(
                bone_directory_name.replace('-', ' ').title(),
                ' in logarithmic scale' if log_scale else ''
            )
        )
        ax.set_xlabel(
            'Area (mm²)'
        )
        ax.set_ylabel(
            'Number of Canal (1)'
        )

    for log_scale in [False, True]:
        plt.figure()
        g = sns.FacetGrid(
            df,
            col="Group",
            sharex=False,
            sharey=False,
            height=5  # Large to accommodate column name, see https://github.com/mwaskom/seaborn/issues/1833
        )
        g.map_dataframe(
            sns.histplot,
            x="Area",
            hue="Location",
            bins=10,
            log_scale=log_scale
        )
        g.set_titles(
            col_template="{col_name}",
            row_template="{row_name}"
        )
def test_hypothesis(df, column_name, one_tailed=True, bone_region_slug=''):
    """Visualise data and use Mann-Whitney U test to validate hypothesis."""  
    # Visualise data
    sns.catplot(
        data=df,
        kind="swarm",
        x="Group",
        y=column_name
    ).fig.suptitle(
        '{} for {}'.format(
            column_name,
            bone_region_slug.replace('-', ' ').title()
        )
    )

    # Mann-Whitney U test
    rested_group = df.loc[df["Group"] == 'Rested', column_name]
    exercised_group = df.loc[df["Group"] == 'Exercised', column_name]
    
    if one_tailed:
        # For Level of significance: 5% (P = 0.05) one-tailed
        if rested_group.shape[0] == 6 and exercised_group.shape[0] == 6:
            critical_value = 7
        elif (rested_group.shape[0] == 5 and exercised_group.shape[0] == 6) or (rested_group.shape[0] == 6 and exercised_group.shape[0] == 5):
            critical_value = 5
        else:
            critical_value = 0
    else:
        # For Level of significance: 5% (P = 0.05) two-tailed
        if rested_group.shape[0] == 6 and exercised_group.shape[0] == 6:
            critical_value = 5
        elif (rested_group.shape[0] == 5 and exercised_group.shape[0] == 6) or (rested_group.shape[0] == 6 and exercised_group.shape[0] == 5):
            critical_value = 3
        else:
            critical_value = 0
    try:
        statistic, pvalue = scipy.stats.mannwhitneyu(
            exercised_group,
            rested_group,
            use_continuity=True,
            alternative='less'
        )
    except ValueError as err:
        statistic = float('inf')
    
    if statistic <= critical_value:
        print(
            """Median for exercised group is {:f} and median for rested group is {:f}."""
            """\n"""
            """We decide to reject the null hypothesis """
            """because U = {} is less than or equal the critical value of {} (p < 0.05 {})."""
            """\n"""
            """The distributions of {} for exercised and rested group differed significantly!"""
            """""".format(
                exercised_group.median(),
                rested_group.median(),
                statistic,
                critical_value,
                "one-tailed" if one_tailed else 'two-tailed',
                column_name
            )
        )
    else:
        print(
            """Median for exercised group is {:f} and median for rested group is {:f}."""
            """\n"""
            """We decide to retain the null hypothesis """
            """because U = {} is greater than the critical value of {} (p < 0.05 {})."""
            """\n"""
            """The distributions of {} for exercised and rested group is similar!"""           
            """""".format(
                exercised_group.median(),
                rested_group.median(),
                statistic,
                critical_value,
                "one-tailed" if one_tailed else 'two-tailed',
                column_name
            )
        )
    
    return statistic
