# Microradiography Faxitron

Preprint: https://www.biorxiv.org/content/10.1101/2022.05.06.490923v1

## Manuscript

- HTML version with [Hypothesis](https://web.hypothes.is/) enabled: https://raniere-phd.gitlab.io/microradiography-faxitron/article.html

## Recommended Reading List

For anyone new to the field,
we recommend to read

- Ott, Susan M. ‘Cortical or Trabecular Bone: What’s the Difference?’ American Journal of Nephrology, vol. 47, no. 6, 2018, pp. 373–75, doi:10.1159/000489672.
- Felder, A. A., et al. ‘Secondary Osteons Scale Allometrically in Mammalian Humerus and Femur’. Royal Society Open Science, vol. 4, Oct. 2017, p. 19, doi:10.1098/rsos.170431.

## Setup

Follow the instructions to have a working repository.

### Raw Data

Raw data is in `data-raw`.
Raw data include

- `.tif` images
- `.png` ROI mask
- `.csv` metadata

### Data

The raw data need some pre-processing
before be ready to analysis.
Run

```
$ make data data/test
```

to populate the directory `data`.

## Experiments

We use [MLflow](https://mlflow.org) to manage our experiments.

### Dependencies

Run

```
$ conda env create -f environment.yml
```

to install the Python dependencies.

### Experiment Anatomy

See `main.py`.

### Run

```
$ mlflow run . -P gui=1
```

ImageJ requires a display server (see FAQ).

To pass parameters,
use

```
$ mlflow run . -P name=value -P name=value
```

### Results

Run

```
$ mlflow ui
```

to launch MLflow built-in Tracking UI
and access it visiting http://localhost:5000.

### Experiment Report

We use Jupyter Notebook for analyse the experiment results.
The Jupyter Notebook will be executed at the end of each experiment.
Open `report.nbconvert.html` to read it.

## FAQ

1.  Why can I not use headless mode?
    Or why do I need a graphical user interface?

    Because ROI Manager does **not** work headless,
    see https://github.com/imagej/imagej-legacy/issues/153.
2.  What is `%%writefile shared-functions.py` in some cells?

    It is a Jupyter magic to write the content of the cell to a file.
    We use it to export the Python code to a `.py` file
    and
    reuse it in other Jupyter Notebooks.
3.  What is `%run shared-functions.py`?

    It is a Jupyter magic to execute the content of the file
    inside the Notebook's kernel.
    We use it to load functions from `.py` file.
4.  What is `%store all_bone_df all_resorption_canal_df`?

    It is a Jupyter magic to save the variables' values.
    We use it to **provide** data to another Jupyter notebooks.
5.  What is `%store -r`?

    It is a Jupyter magic to load the variables' values.
    We use it to **read** data from another Jupyter notebook.
