/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will create some test images to assert the quality of resorption canal counting.
*/

headless_str_arg = getArgument();

headless_args = split(headless_str_arg);

if(headless_args.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_args[0].endsWith("/")) {
        root_path = headless_args[0];
    }
    else {
        root_path = headless_args[0] + "/";
    }
}

source_path = root_path + "data/";
destination_path = root_path + "data/test/";

print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);

File.makeDirectory(destination_path)

open(source_path + "right-metacarpal-3-mid-diaphyseal/09.tif");
makeRectangle(629, 323, 250, 250);
run("Crop");
saveAs("Tiff", destination_path + "noise.tif");
makeRectangle(1, 1, 248, 248);
roiManager("Add");
roiManager("Save", destination_path + "noise.periosteum.roi");
roiManager("Save", destination_path + "noise.endosteum.roi");
roiManager("Select", 0);
roiManager("Delete");

open(source_path + "right-metacarpal-3-distal-diaphysis-lateral-aspect/01.tif");
makeRectangle(1659, 1300, 250, 250);
run("Crop");
saveAs("Tiff", destination_path + "white.tif");
makeRectangle(1, 1, 248, 248);
roiManager("Add");
roiManager("Save", destination_path + "white.periosteum.roi");
roiManager("Save", destination_path + "white.endosteum.roi");
roiManager("Select", 0);
roiManager("Delete");

open(source_path + "right-metacarpal-3-distal-diaphysis-lateral-aspect/09.tif");
makeRectangle(1652, 1064, 250, 250);
run("Crop");
saveAs("Tiff", destination_path + "cut-marks.tif");
makeRectangle(1, 1, 248, 248);
roiManager("Add");
roiManager("Save", destination_path + "cut-marks.periosteum.roi");
roiManager("Save", destination_path + "cut-marks.endosteum.roi");
roiManager("Select", 0);
roiManager("Delete");

open(source_path + "left-tibia-distal-third/06.tif");
makeRectangle(1151, 520, 250, 250);
run("Crop");
saveAs("Tiff", destination_path + "dark.tif");
makeRectangle(1, 1, 248, 248);
roiManager("Add");
roiManager("Save", destination_path + "dark.periosteum.roi");
roiManager("Save", destination_path + "dark.endosteum.roi");
roiManager("Select", 0);
roiManager("Delete");
