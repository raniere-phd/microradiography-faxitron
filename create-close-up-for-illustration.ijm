/*
ImageJ/Fiji macro for Microradiography Faxitron

This macro will crop one image to use as close-up to illustrate secondary osteons.
*/

headless_str_arg = getArgument();

headless_args = split(headless_str_arg)

if(headless_args.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_args[0].endsWith("/")) {
        root_path = headless_args[0];
    }
    else {
        root_path = headless_args[0] + "/";
    }
}

function save_close_up(source_dir, source_suffix, destination_dir, destination_suffix) {
    basename_source = "left-10th-rib/07.";
    basename_destination = "workflow.";

    source_path = root_path + source_dir;
    destination_path = root_path + destination_dir;

    print("Root path: " + root_path);
    print("Source path: " + source_path);
    print("Destination path: " + destination_path);

    open(source_path + basename_source + source_suffix);
    makeRectangle(1383, 423, 65, 65);
    run("Crop");
    run("Canvas Size...", "width=65 height=85 position=Bottom-Left");
    run("Set Scale...", "distance=100 known=1 unit=mm");
    run("Duplicate...", " ");

    run("Scale Bar...", "width=0.1 height=14 font=10 color=White background=None location=[Upper Right] bold hide overlay");
    saveAs(
	"PNG",
	destination_path + basename_destination + destination_suffix + "close-up.png"
    );
    close();
}

save_close_up("data/", "tif", "img/", "");
save_close_up("data-augmented/", "binary.png", "img/", "binary.");
save_close_up("data-augmented/", "resorption.canal.xray.png", "img/", "resorption.canal.xray.");

source_path = root_path + "data/";
destination_path = root_path + "img/";

open(source_path + "left-10th-rib/11.tif");

/* Get measure to set scale later */
makeLine(100, 100, 200, 100);
run("Measure");
distance = getResult("Length", 0);

run("Duplicate...", "resorption");
makeRectangle(649, 1663, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-resorption.png");
close();
run("Duplicate...", "formation");
makeRectangle(993, 514, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-formation.png");
close();
makeRectangle(1428, 355, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-completed.png");
close();

source_path = root_path + "data-augmented/";
destination_path = root_path + "img/";

open(source_path + "left-10th-rib/11.binary.png");

/* Set scale */
run("Set Scale...", "distance=100 known=" + distance + " unit=mm");

run("Duplicate...", "resorption");
makeRectangle(649, 1663, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-resorption.binary.png");
close();
run("Duplicate...", "formation");
makeRectangle(993, 514, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-formation.binary.png");
close();
makeRectangle(1428, 355, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-completed.binary.png");
close();

open(source_path + "left-10th-rib/11.resorption.canal.binary.png");

/* Set scale */
run("Set Scale...", "distance=100 known=" + distance + " unit=mm");

run("Duplicate...", "resorption");
makeRectangle(649, 1663, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-resorption.canal.binary.png");
close();
run("Duplicate...", "formation");
makeRectangle(993, 514, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-formation.canal.binary.png");
close();
makeRectangle(1428, 355, 50, 50);
run("Crop");
run("Canvas Size...", "width=50 height=60 position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=8 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "osteon-completed.canal.binary.png");
close();

/* Close-up to trabecularised canal */

open(root_path + "data/left-10th-rib/06.tif");
makeRectangle(0, 1177, 200, 200);
run("Crop");
run("Size...", "width=2368 height=2340 depth=1 constrain average interpolation=Bilinear");
run("Canvas Size...", "width=" + getWidth() + " height=" + (getHeight() + 160) + " position=Bottom-Right");
run("Scale Bar...", "width=0.1 height=80 font=84 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "trabecularised-close-up.png");
close();

/* Close-up to humerus */

open(root_path + "data/left-proximal-humerus-proximal-to-deltoid-tuberosity/04.tif");
makeRectangle(604, 201, 400, 400);
run("Crop");
run("Canvas Size...", "width=400 height=420 position=Bottom-Left");
run("Scale Bar...", "width=5 height=14 font=10 color=White background=None location=[Upper Right] bold hide overlay");
saveAs("PNG", destination_path + "humerus-cortical-vs-trabecular-close-up.png");
close();
